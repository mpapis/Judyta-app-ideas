class User < ApplicationRecord
  has_one :role
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def admin?
    role.name == "admin"
  end
end